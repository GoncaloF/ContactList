//
//  ContactListApp.swift
//  ContactList
//
//  Created by Gonçalo Feliciano on 02/06/2022.
//

import SwiftUI

@main
struct ContactListApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
              
        }
    }
}
