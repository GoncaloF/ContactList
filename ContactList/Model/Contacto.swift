//
//  Contacto.swift
//  ContactList
//
//  Created by Gonçalo Feliciano on 02/06/2022.
//

import Foundation
import SwiftUI


class Contacto:Identifiable{
    var id = UUID()
    
    var nome:String
    var apelido:String?
    var tlm:Int
    var email:String?
    var foto:Image
    var cats:Set<Categoria>
    
    
    init(nome:String,apelido:String? = nil,tlm:Int, email:String? = nil, foto:Image? = nil ,cat:Categoria? = nil){
        
        var cats:Set<Categoria> = Set<Categoria>()
        
        if let catNoOpt = cat{
            cats.insert(catNoOpt)
        }
        
        self.nome = nome
        self.apelido = apelido
        self.tlm = tlm
        self.email = email
        self.foto = foto ?? Image("noImg")
        self.cats = cats
    }
    
    
    init(nome:String,apelido:String? = nil,tlm:Int, email:String? = nil, foto:Image? = nil ,cats:Set<Categoria> = Set<Categoria>()){
        self.nome = nome
        self.apelido = apelido
        self.tlm = tlm
        self.email = email
        self.foto = foto ?? Image("noImg")
        self.cats = cats
    }
    
    func addCat(cat:Categoria) throws {
    
        let resp = self.cats.insert(cat)
    
        if !resp.inserted{
            throw Erros.CategoriaJaAdicionada
        }
    }
    
}
